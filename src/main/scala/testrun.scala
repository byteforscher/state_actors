// DEPREACTED already

import scala.concurrent.Await
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

object TestRun {
  implicit val timeout = Timeout(5 seconds)

  def whereIsEveryOne(cities: List[ActorRef]) = {
    cities.foreach { city =>
      val name = Await.result(city ? Name, timeout.duration).asInstanceOf[String]
      val r = Await.result(city ? All, timeout.duration).asInstanceOf[List[ActorRef]]
      val reprFuture = r.map { _ ? Repr }
      val reprs = for { repr <- reprFuture } yield (Await.result(repr, timeout.duration).asInstanceOf[PersonRepr])

      println(s"${reprs.length} Residents in ${name}: ${reprs.map { _.name }.mkString(", ") }")
    }
  }

  def start(system: akka.actor.ActorSystem) = {
    val persons = List("felix", "earl", "dude", "lady", "fred").map { name =>
      system.actorOf(Props(new Person(name = name)), name = s"person_$name")
    }

    val cities = List("berlin", "cologne").map { name =>
      system.actorOf(Props(new City(name = name)), name = s"city_$name")
    }

    println("*** Everybody is moving to berlin")
    persons.foreach { person =>
      cities.head ! new MovingIntoTown(person)
    }
    whereIsEveryOne(cities)

    println("*** Moving to nowhere")
    val a_person = persons.head
    val a_city = cities.head
    a_city ! new MovingAway(a_person)
    whereIsEveryOne(cities)

    println("*** Moving somewhere else")
    val b_person = persons.last
    val b_city = cities.last
    a_city ! new MovingAway(b_person)
    b_city ! new MovingIntoTown(b_person)
    whereIsEveryOne(cities)

    println(s"*** Does fred live in cologne?")
    val b_result = Await.result( b_city ? new Search("fred"), timeout.duration).asInstanceOf[Option[ActorRef]]
    b_result match {
      case Some(person) => println("YES")
      case None => println("Nope!")
    }

    println(s"*** Does fred live in berlin?")
    val a_result = Await.result( a_city ? new Search("fred"), timeout.duration).asInstanceOf[Option[ActorRef]]
    a_result match {
      case Some(person) => println("YES")
      case None => println("Nope!")
    }

    println("*** Where does fred live?")
    val maybeFredsCity = cities.find { city =>
      Await.result( city ? new Search("fred"), timeout.duration).asInstanceOf[Option[ActorRef]] match {
        case Some(_) => true
        case None => false
      }
    }

    maybeFredsCity match {
      case Some(fredsCity) => {
        val fredsCityRepr = Await.result( fredsCity ? Repr, timeout.duration).asInstanceOf[CityRepr]
        println(s"fred lives in ${fredsCityRepr.name}")
      }
      case None => println("Sadly fred is homeless")
    }

    println("*** Who is still living in berlin?")
    val berlinResidents = Await.result( a_city ? All, timeout.duration).asInstanceOf[List[ActorRef]]
    val berlinResidentsReprs = for { repr <- berlinResidents.map { _ ? Repr } } yield (Await.result(repr, timeout.duration).asInstanceOf[PersonRepr])
    println(s"Residents: ${berlinResidentsReprs.map { _.name }.mkString(", ")}")

    println("*** lady is becoming gal now")
  }
}
