import scala.concurrent.Await
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import akka.io.IO
import spray.can.Http
import scala.concurrent.duration._

object Boot extends App {
  implicit val system = ActorSystem("CityInformationSystem")
  implicit val timeout = Timeout(5 seconds)

  val p = system.actorOf(Props[Persons], "all-persons")
  val c = system.actorOf(Props[Cities], "all-cities")

  List("felix", "earl", "dude", "lady", "fred").foreach { name =>
    p ! new Create(name)
  }

  List("berlin", "cologne").map { name =>
    c ! new Create(name)
  }

  val someContext = new SomeContext {
    override val persons: ActorRef = p
    override val cities: ActorRef = c
  }

  val api = system.actorOf(Props( new MyApiActor(someContext)), "api-dummy")
  IO(Http) ? Http.Bind(api, interface = "localhost", port = 9000)

  //TestRun.start(system)
  //Thread.sleep(250)
  //println("stopping akka")
  //system.shutdown()
}
