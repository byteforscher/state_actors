import scala.concurrent.{Await, Future}
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.util.{Try, Success, Failure}

import spray.can.Http
import spray.routing._
//import spray.routing.directives.CachingDirectives
import spray.http._
import spray.json._
import DefaultJsonProtocol._
import MediaTypes._
import StatusCodes._

import MyJsonProtocol._

class MyApiActor(c: SomeContext) extends Actor with MyApi {
  implicit def actorRefFactory = context
  def receive = runRoute(myRoute)

  override val someContext = c
}

trait MyApi extends HttpService { this: MyApiActor =>

  val someContext: SomeContext

  implicit def executionContext = actorRefFactory.dispatcher
  implicit val system = context.system
  implicit val timeout: Timeout = Timeout(30.seconds)

  val myRoute = {
    get {
      pathSingleSlash {
        complete("welcome stanger!")
      } ~
      path("ping") {
        complete {
          (OK, "PONG")
        }
      } ~
      path("cities") {
        respondWithMediaType(`application/json`) {
          complete {
            for {
              cities <- someContext.cities ? All
              reprs <- Future.sequence( cities.asInstanceOf[List[ActorRef]].map { _ ? Repr } )
            } yield {
              (OK, s"${reprs.asInstanceOf[List[CityRepr]].toJson}")
            }
          }
        }
      } ~
      path("cities" / Segment / "residents") { (name) =>
        respondWithMediaType(`application/json`) {
          complete {

            for {
              maybeCity <- someContext.cities ? new Get(name)
              residents <- maybeCity.asInstanceOf[Option[ActorRef]].get ? All // guard
              residentsReprs <- Future.sequence( residents.asInstanceOf[List[ActorRef]].map { _ ? Repr } )
            } yield {
              (maybeCity, residents, residentsReprs) match {
                case (Some(city), residents, residentsReprs) => {
                  (OK, s"${residentsReprs.asInstanceOf[List[PersonRepr]].toJson}")
                }
                case _ => (NotFound, ":/")
              }
            }
          }
        }
      } ~
      path("cities" / Segment / "residents" / "add" / Segment) { (cityName, personName) =>
        complete {
          for {
            city <- someContext.cities ? new Get(cityName)
            person <- someContext.persons ? new Get(personName)
          } yield {
            (city, person) match {
              case (Some(city), Some(person)) => {
                city.asInstanceOf[ActorRef] ! new MovingIntoTown(person.asInstanceOf[ActorRef])
                (Accepted, "will be processed soonish")
              }
              case _ => (NotFound, s"$cityName or $personName does not exist!")
            }
          }
        }
      } ~
      path("cities" / Segment / "residents" / "remove" / Segment) { (cityName, personName) =>
        // dry this up with _add_
        complete {
          for {
            city <- someContext.cities ? new Get(cityName)
            person <- someContext.persons ? new Get(personName)
          } yield {
            (city, person) match {
              case (Some(city), Some(person)) => {
                city.asInstanceOf[ActorRef] ! new MovingAway(person.asInstanceOf[ActorRef])
                (Accepted, "will be processed soonish")
              }
              case _ => (NotFound, s"$cityName or $personName does not exist!")
            }
          }
        }
      } ~
      path("cities" / "search" / Segment) { (query) =>

        respondWithMediaType(`application/json`) {
          complete {

            for {
              cities <- someContext.cities ? All
              results <- Future.sequence( cities.asInstanceOf[List[ActorRef]].map { _ ? new Search(query) } )
            } yield {

              val hits = results.asInstanceOf[List[Option[ActorRef]]].filter {
                _ match {
                  case Some(_) => true
                  case _ => false
                }
              }

              val reprFuture = hits.map { _.get}.map { _ ? Repr }
              val reprs = for { repr <- reprFuture } yield (Await.result(repr, timeout.duration).asInstanceOf[CityRepr])

              (OK, s"${reprs.toJson}")
            }
          }
        }
      } ~
      path("persons") {
        respondWithMediaType(`application/json`) {
          complete {
            for {
              persons <- someContext.persons ? All
              personReprs <- Future.sequence( persons.asInstanceOf[List[ActorRef]].map { _ ? Repr } )
            } yield {
              (OK, s"${personReprs.asInstanceOf[List[PersonRepr]].toJson}")
            }

          }
        }
      } ~
      path("persons" / Segment) { (name) =>
        respondWithMediaType(`application/json`){
          complete {
            val selection = system.actorSelection(s"/user/all-persons/person_$name")
            val future = selection.resolveOne().map( Some(_) ).recover {
              case ex: ActorNotFound => None
            }

            future.map {
              _ match {
                case Some(person) => {
                  val personRepr = Await.result( person ? Repr, timeout.duration).asInstanceOf[PersonRepr]
                  (OK, s"${personRepr.toJson}")
                }
                case None => (NotFound, "does not exist!")
              }
            }
          }
        }
      }
    }
  }
}
