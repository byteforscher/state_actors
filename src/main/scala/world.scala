import scala.concurrent.Await
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

import spray.json._
import DefaultJsonProtocol._

case object StartMessage
case object StopMessage
case object Repr
case object All
case object Name
case class Search(query: String)

case class Get(name: String)
case class Create(name: String)

case class MovingIntoTown(person: ActorRef)
case class MovingAway(person: ActorRef)

trait SomeContext {
  val persons: ActorRef
  val cities: ActorRef
}

class Persons extends Actor {

  implicit val timeout: Timeout = Timeout(30.seconds)

  val persons = collection.mutable.Map[String, ActorRef]()

  def receive = {
    case All => {
      sender ! persons.values.map { p => p }
    }
    case Get(name: String) => {
      persons.contains(name) match {
        case true => sender ! persons.get(name)
        case false => sender ! None
      }
    }
    case Create(name: String) => {
      println(s"Persons.create: $name")
      val person = context.actorOf(Props(new Person(name = name)), name = s"person_$name")
      persons(name) = person
    }
  }
}

case class PersonRepr(name: String, address: Option[String] = None, city: Option[String] = None)
class Person(name: String, address: Option[String] = None, city: Option[String] = None) extends Actor {

  implicit val timeout: Timeout = Timeout(30.seconds)

  var repr = new PersonRepr(name, address, city)

  def receive = {
    case Repr => {
      sender ! repr
    }
    case StopMessage => {
      println(s"-> ${name} is going down")
      context.stop(self)
    }
  }
}

class Cities extends Actor {

  implicit val timeout: Timeout = Timeout(30.seconds)

  val cities = collection.mutable.Map[String, ActorRef]()

  def receive = {
    case All => {
      sender ! cities.values.map { p => p }
    }
    case Create(name: String) => {
      println(s"Cities.create: $name")
      val city = context.actorOf(Props(new City(name = name)), name = s"city_$name")
      cities(name) = city
    }
    case Get(name: String) => {
      // Not Working: sender ! cities.getOrElse(name, None)
      cities.contains(name) match {
        case true => sender ! cities.get(name)
        case false => sender ! None
      }
    }
  }
}


case class CityRepr(name: String)
class City(name: String) extends Actor {

  implicit val timeout: Timeout = Timeout(30.seconds)

  var residents = collection.mutable.Map[String, ActorRef]()

  def receive = {
    case Name => {
      sender ! name
    }
    case Repr => {
      sender ! new CityRepr(name)
    }
    case StopMessage => {
      println("City servies are closing for today")
      context.stop(self)
    }
    case MovingIntoTown(person: ActorRef) => {
      val repr = Await.result(person ? Repr, timeout.duration).asInstanceOf[PersonRepr]
      println(s"${repr.name} is moving to $name")
      residents(repr.name) = person
    }
    case MovingAway(person: ActorRef) => {
      val personRepr = Await.result(person ? Repr, timeout.duration).asInstanceOf[PersonRepr]
      println(s"${personRepr.name} is leaving $name :/")
      residents.remove(personRepr.name)
    }
    case All => {
      sender ! residents.values.map { p => p }
    }
    case Search(query: String) => {
      residents.contains(query) match {
        case true => sender ! Some(self)
        case false => sender ! None
      }
    }
  }
}

object MyJsonProtocol extends DefaultJsonProtocol {

  implicit object PersonReprJsonWriter extends RootJsonWriter[PersonRepr] {
    def write(p: PersonRepr) = JsObject(
      "name" -> JsString(p.name),
      "address" -> JsString(p.address.getOrElse("")),
      "city" -> JsString(p.city.getOrElse(""))
    )
  }

  // why is this not covered by:
  // https://github.com/spray/spray-json/blob/master/src/main/scala/spray/json/CollectionFormats.scala#L25
  implicit object PersonReprsJsonWriter extends RootJsonWriter[List[PersonRepr]] {
    def write(list: List[PersonRepr]) = JsArray(list.map(_.toJson).toVector)
  }

  implicit object CityReprJsonWriter extends RootJsonWriter[CityRepr] {
    def write(p: CityRepr) = JsObject(
      "name" -> JsString(p.name)
    )
  }

  implicit object CityReprsJsonWriter extends RootJsonWriter[List[CityRepr]] {
    def write(list: List[CityRepr]) = JsArray(list.map(_.toJson).toVector)
  }

}
