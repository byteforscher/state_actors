name := "Akka Example"

version := "0.0.1"

scalaVersion := "2.11.4"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

mainClass in (Compile,run) := Some("Boot")

libraryDependencies ++= {
  val akkaV = "2.3.8"
  val sprayV = "1.3.2"
  Seq(
    "io.spray"          %% "spray-can"     % sprayV,
    "io.spray"          %% "spray-routing" % sprayV,
    "io.spray"          %% "spray-json"    % "1.3.1",
    "com.typesafe.akka" %% "akka-actor"    % akkaV
  )
}

Revolver.settings
