*Playing around with stateful actors*

## TODO

* [Provide http api](http://spray.io)
* Add tests
* Cassandra persistence
* [Become reactive]( https://github.com/typesafehub/activator-akka-stream-scala#master)
* Run distributed
* [Split api](http://stackoverflow.com/questions/14653526/can-spray-io-routes-be-split-into-multiple-controllers)

## Tricks

    http://alvinalexander.com/java/jwarehouse/akka-2.3/akka-actor/src/main/scala/akka/actor/ActorSelection.scala.shtml
    system.actorSelection("person_felix")

    Actor: self.path.name
